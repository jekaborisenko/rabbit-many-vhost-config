package com.example.rabbitconf.rabbitconf.listener;

import com.example.rabbitconf.rabbitconf.model.DataModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ServiceOneListener {

    @RabbitListener(queues = "${external.url.service-one.one-queue-accept}", containerFactory = "rabbitListenerContainerFactory")
    public void receiveMessage(DataModel dataModel) {
        log.info("Info Received: " +
                "\n\tRequestId: " + dataModel.getRequestId());
    }
}
