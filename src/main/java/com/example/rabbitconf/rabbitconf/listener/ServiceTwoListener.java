package com.example.rabbitconf.rabbitconf.listener;

import com.example.rabbitconf.rabbitconf.model.DataModelExtended;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ServiceTwoListener {

    @RabbitListener(queues = "${external.url.service-two.two-queue-accept}", containerFactory = "rabbitListenerContainerFactory2")
    public void receiveMessage(DataModelExtended dataModelExtended) {
        log.info("Info Received: " +
                "\n\tRequestId: " + dataModelExtended.getRequestId() +
                "\n\tUsername: " + dataModelExtended.getUsername());
    }
}
