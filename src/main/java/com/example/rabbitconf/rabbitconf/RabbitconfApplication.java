package com.example.rabbitconf.rabbitconf;

import com.example.rabbitconf.rabbitconf.service.ServiceMain;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;

@RequiredArgsConstructor
@SpringBootApplication(exclude = {RabbitAutoConfiguration.class})
public class RabbitconfApplication {

    private final ServiceMain serviceMain;

    public static void main(String[] args) {
        SpringApplication.run(RabbitconfApplication.class, args);
        System.out.println("Application [RabbitconfApplication] started!");
    }
}
