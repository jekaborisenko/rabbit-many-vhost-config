package com.example.rabbitconf.rabbitconf.config.rabbit;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.springframework.amqp.core.BindingBuilder.bind;

@Configuration
public class RabbitServiceTwoConfig {

    private static final String TWO_EXCHANGE = "${external.url.service-two.two-exchange}";
    private static final String TWO_QUEUE_ACCEPT = "${external.url.service-two.two-queue-accept}";

    @Value(TWO_EXCHANGE)
    private String exchange;

    @Value(TWO_QUEUE_ACCEPT)
    private String queueAccept;

    private final RabbitAdmin rabbitAdmin;

    @Autowired
    public RabbitServiceTwoConfig(@Qualifier("serviceTwoRabbitAdmin") RabbitAdmin rabbitAdmin) {
        this.rabbitAdmin = rabbitAdmin;
    }

    @Bean
    public FanoutExchange serviceTwoExchange() {
        FanoutExchange fanoutExchange = new FanoutExchange(exchange);
        rabbitAdmin.declareExchange(fanoutExchange);
        return fanoutExchange;
    }

    @Bean
    public Queue serviceTwoAcceptQueue() {
        Queue queue = new Queue(queueAccept);
        rabbitAdmin.declareQueue(queue);
        return queue;
    }

    @Bean
    public Binding serviceTwoQueueBinding() {
        Binding binding = bind(serviceTwoAcceptQueue()).to(serviceTwoExchange());
        rabbitAdmin.declareBinding(binding);
        return binding;
    }
}
