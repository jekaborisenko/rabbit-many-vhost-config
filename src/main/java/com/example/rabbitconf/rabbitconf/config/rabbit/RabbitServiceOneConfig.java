package com.example.rabbitconf.rabbitconf.config.rabbit;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.springframework.amqp.core.BindingBuilder.bind;

@Configuration
public class RabbitServiceOneConfig {

    private static final String ONE_EXCHANGE = "${external.url.service-one.one-exchange}";
    private static final String ONE_QUEUE_ACCEPT = "${external.url.service-one.one-queue-accept}";

    @Value(ONE_EXCHANGE)
    private String exchange;

    @Value(ONE_QUEUE_ACCEPT)
    private String queueAccept;

    private final RabbitAdmin rabbitAdmin;

    @Autowired
    public RabbitServiceOneConfig(@Qualifier("serviceOneRabbitAdmin") RabbitAdmin rabbitAdmin) {
        this.rabbitAdmin = rabbitAdmin;
    }

    @Bean
    public FanoutExchange serviceOneExchange() {
        FanoutExchange fanoutExchange = new FanoutExchange(exchange);
        rabbitAdmin.declareExchange(fanoutExchange);
        return fanoutExchange;
    }

    @Bean
    public Queue serviceOneAcceptQueue() {
        Queue queue = new Queue(queueAccept);
        rabbitAdmin.declareQueue(queue);
        return queue;
    }

    @Bean
    public Binding serviceOneQueueBinding() {
        Binding binding = bind(serviceOneAcceptQueue()).to(serviceOneExchange());
        rabbitAdmin.declareBinding(binding);
        return binding;
    }
}
