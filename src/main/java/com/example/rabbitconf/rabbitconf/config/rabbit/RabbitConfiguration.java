package com.example.rabbitconf.rabbitconf.config.rabbit;

import com.example.rabbitconf.rabbitconf.config.property.RabbitConnectionProperties;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.ConditionalRejectingErrorHandler;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.messaging.MessageHandlingException;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.ErrorHandler;

@EnableRabbit
@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(RabbitConnectionProperties.class)
public class RabbitConfiguration {

    private final RabbitConnectionProperties rabbitConnectionProperties;
    private final Environment env;


    private static ConnectionFactory configureConnectionFactory(String host, Integer port, String virtualHost, String username, String password) {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host, port);
        if (StringUtils.isNotEmpty(virtualHost)) connectionFactory.setVirtualHost(virtualHost);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        return connectionFactory;
    }

    @Bean
    public ConnectionFactory serviceOneConnectionFactory() {
        RabbitConnectionProperties.ConnectionProperty cp = rabbitConnectionProperties.getOne();
        return configureConnectionFactory(
                cp.getHost(),
                cp.getPort(),
                cp.getVirtualHost(),
                cp.getUsername(),
                cp.getPassword());
    }

    @Bean
    public RabbitAdmin serviceOneRabbitAdmin() {
        RabbitAdmin rabbitAdmin = new RabbitAdmin(serviceOneConnectionFactory());
        // Documentation: By default, all queues, exchanges, and bindings are declared
        // by all RabbitAdmin instances (that have auto-startup="true") in the application context.
        rabbitAdmin.setAutoStartup(false);
        return rabbitAdmin;
    }

    // Container factory для service one. Должен называться именно rabbitListenerContainerFactory, потому что должен быть хотябы один бин с таким имененм
    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(serviceOneConnectionFactory());
        factory.setMessageConverter(jackson2JsonMessageConverter());
        factory.setTaskExecutor(taskExecutor());
        factory.setErrorHandler(errorHandler());
        return factory;
    }

    @Bean
    public ConnectionFactory serviceTwoConnectionFactory() {
        RabbitConnectionProperties.ConnectionProperty cp = rabbitConnectionProperties.getTwo();
        return configureConnectionFactory(
                cp.getHost(),
                cp.getPort(),
                cp.getVirtualHost(),
                cp.getUsername(),
                cp.getPassword());
    }

    @Bean
    public RabbitAdmin serviceTwoRabbitAdmin() {
        RabbitAdmin rabbitAdmin = new RabbitAdmin(serviceTwoConnectionFactory());
        rabbitAdmin.setAutoStartup(false);
        return rabbitAdmin;
    }

    // Container factory для service two. На имя похуй
    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory2() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(serviceTwoConnectionFactory());
        factory.setMessageConverter(jackson2JsonMessageConverter());
        factory.setTaskExecutor(taskExecutor());
        factory.setErrorHandler(errorHandler());
        return factory;
    }

    @Bean
    public Jackson2JsonMessageConverter jackson2JsonMessageConverter() {
        return new Jackson2JsonMessageConverter(Jackson2ObjectMapperBuilder.json()
                .modules(new JavaTimeModule())
                .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .build());
    }

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor scheduledExecutorService = new ThreadPoolTaskExecutor();
        scheduledExecutorService.setCorePoolSize(env.getProperty("listener.pool.core-size", Integer.class, 20));
        scheduledExecutorService.setMaxPoolSize(env.getProperty("listener.pool.max-size", Integer.class, 40));
        scheduledExecutorService.setThreadNamePrefix("listener-executor-");
        return scheduledExecutorService;
    }

    @Bean
    public ErrorHandler errorHandler() {
        return new ConditionalRejectingErrorHandler(new CustomErrorHandler());
    }

    private static class CustomErrorHandler extends ConditionalRejectingErrorHandler.DefaultExceptionStrategy {
        @Override
        protected boolean isUserCauseFatal(Throwable cause) {
            return cause instanceof MessageHandlingException;
        }
    }
}
