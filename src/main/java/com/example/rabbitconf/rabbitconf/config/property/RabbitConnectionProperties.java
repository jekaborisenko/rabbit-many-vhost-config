package com.example.rabbitconf.rabbitconf.config.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.rabbitmq.client.ConnectionFactory.DEFAULT_AMQP_PORT;

@Getter
@Setter
@ConfigurationProperties("spring.rabbitmq")
public class RabbitConnectionProperties {

    private ConnectionProperty one;
    private ConnectionProperty two;

    @Getter
    @Setter
    public static class ConnectionProperty {
        private String username;
        private String password;
        private String host;

        public int getPort() {
            if (port <= 0) {
                return DEFAULT_AMQP_PORT;
            }
            return port;
        }

        private int port;
        private String virtualHost;
    }
}
