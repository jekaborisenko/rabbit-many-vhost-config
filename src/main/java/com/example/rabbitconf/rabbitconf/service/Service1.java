package com.example.rabbitconf.rabbitconf.service;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class Service1 {

    private final Service2 service2;

    @Transactional
    public void transferMoney() {
        service2.methodA();
    }
}
