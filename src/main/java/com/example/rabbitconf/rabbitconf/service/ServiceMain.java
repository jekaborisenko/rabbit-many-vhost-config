package com.example.rabbitconf.rabbitconf.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;


@Service
@RequiredArgsConstructor
public class ServiceMain {

    private final Service1 service1;

    @PostConstruct
    private void postConstruct(){
        service1.transferMoney();
    }
}
