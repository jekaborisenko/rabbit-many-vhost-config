package com.example.rabbitconf.rabbitconf.service;


import org.springframework.stereotype.Service;

@Service
public class Service2 {

    public void methodA() {
        try {
            methodExc();
        } catch (Exception e) {
            methodB();
        }
    }

    public void methodB() {
    }

    public void methodExc() {
        throw new RuntimeException("Error during credit");
    }
}
