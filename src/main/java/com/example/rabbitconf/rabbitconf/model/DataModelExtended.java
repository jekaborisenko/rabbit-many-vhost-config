package com.example.rabbitconf.rabbitconf.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataModelExtended extends DataModel {
    private String username;
}
